#include "file_manager.h"

int ropenfile(FILE** const  file, char* const filename) {
    *file = fopen(filename, "rb");
    if (*file) return true;
    return false;
}

int wopenfile(FILE** const file, char* const filename) {
    *file = fopen(filename, "wb");
    if (*file) return true;
    return false;
}

int closefile(FILE* const file) {
    int resultClose = fclose(file);
    if (resultClose) return 0;
    return 1;
}

int read(char* const filename,struct image* const image) {
    FILE *file = NULL;
    if (!ropenfile(&file, filename)) return 0;
    if (!read_bmp(file, image)) return 0;
    if (!closefile(file)) return 0;
    return 1;
}

int write(char* const filename, struct image image) {
    FILE *file = NULL;
    if (!wopenfile(&file, filename)) return 0;
    if (!write_bmp(file, &image)) return 0;
    if (!closefile(file)) return 0;
    return 1;
}
