#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FILE_MANAGER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FILE_MANAGER_H

#include "../bmp_manager/read_bmp.h"
#include "../bmp_manager/write_bmp.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

int ropenfile(FILE** const  file, char* const filename);
int wopenfile(FILE** const file, char* const filename);
int read(char* const filename,struct image* const image) ;
int write(char* const filename, struct image const  image);
int closefile(FILE* const file);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_FILE_MANAGER_H
