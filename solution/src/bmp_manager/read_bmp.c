#include "read_bmp.h"

int read_header(FILE* const file, struct bmp_header* const header) {
    if (fread(header, sizeof(struct bmp_header), 1, file) == 1) {
        return 1;
    } else return 0;
}

int read_pixels(FILE* const file, struct image* const image) {
    const uint8_t padding = get_padding(image->width);
    struct pixel *data = image->data;
    const size_t height = image->height;
    const size_t width = image->width;
    for (size_t i = 0; i < height; i++) {
        if (fread(data + width * i, sizeof(struct pixel), width, file) != width) return 0;
        if (fseek(file, padding, SEEK_CUR) != 0) return 0;
    }
    return 1;
}

int read_bmp(FILE* const file, struct image* image) {
    struct bmp_header header = {0};
    if (!read_header(file, &header)) return 0;
    *image = new_image(header.biWidth, header.biHeight);
    if (!read_pixels(file, image)) return 0;

    return 1;
}
