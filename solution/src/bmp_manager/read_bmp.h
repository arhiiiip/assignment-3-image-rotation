#ifndef ASSIGNMENT_3_IMAGE_ROTATION_READ_BMP_H
#define ASSIGNMENT_3_IMAGE_ROTATION_READ_BMP_H

#include "../image_manager/image_manager.h"
#include "bmp.h"
#include <stdbool.h>
#include <stdio.h>

int read_bmp(FILE* const file, struct image* image);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_READ_BMP_H
