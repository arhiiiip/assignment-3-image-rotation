#include "write_bmp.h"
#define TYPE 19778
#define RESERVED 0
#define SIZE 40
#define PLANES 1
#define BITCOUNT 24
#define COMPRESSION 0
#define PELSPERMETER 2834
#define CLRUSED 0
#define CLRIMPORTANT 0

const struct bmp_header NEW_TEMPLATE = {
        .bfType = TYPE,
        .bfReserved = RESERVED,
        .biSize = SIZE,
        .biPlanes = PLANES,
        .biBitCount = BITCOUNT,
        .biCompression = COMPRESSION,
        .biXPelsPerMeter = PELSPERMETER,
        .biYPelsPerMeter = PELSPERMETER,
        .biClrUsed = CLRUSED,
        .biClrImportant = CLRIMPORTANT
};

struct bmp_header new_header(const struct image *image) {
    struct bmp_header new_header = NEW_TEMPLATE;
    uint32_t bmp_header_size = sizeof(struct bmp_header);
    new_header.biWidth = image->width;
    new_header.biHeight = image->height;
    new_header.biSizeImage = image->height * image->width * sizeof(struct pixel);
    new_header.bfileSize = new_header.biSizeImage + sizeof(struct bmp_header);
    new_header.bOffBits = bmp_header_size;
    return new_header;
}


int write_header(FILE* const file, struct bmp_header* const header) {
    if (fwrite(header, sizeof(struct bmp_header), 1, file) == 1) {
        return 1;
    } else return 0;
}

int write_pixels(FILE* const file, struct image const image) {
    const uint8_t padding = get_padding(image.width);
    const size_t height = image.height;
    const size_t width = image.width;
    struct pixel *pixels = image.data;
    const uint8_t paddings[3] = {0};
    for (size_t i = 0; i < height; i++) {
        if (fwrite(pixels + i * width, sizeof(struct pixel) * width, 1, file) == 0) return 0;
        if ((fwrite(paddings, padding, 1, file) == 0) && padding != 0) return 0;
    }
    return 1;
}


int write_bmp(FILE* const file, struct image* image) {
    struct bmp_header header = new_header(image);
    if (!write_header(file, &header)) return 0;
    if (!write_pixels(file, *image)) return 0;
    return 1;
}


