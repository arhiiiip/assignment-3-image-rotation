#include "image_manager.h"
#include <malloc.h>

uint8_t get_padding(const uint32_t width) {
    uint8_t padding = (width * sizeof(struct pixel)) % 4;
    if (padding) padding = 4 - padding;
    return padding;
}

struct image new_image(const size_t width, const size_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(height * width * sizeof(struct pixel))
    };
}

void free_image(struct image const image) {
    free(image.data);
}

int image_rotator(struct image const oldImage, struct image* newImage) {
    *newImage = new_image( oldImage.height, oldImage.width);
    for (size_t i = 0; i < oldImage.width; i++) {
        for (size_t j = 0; j < oldImage.height; j++) {
            newImage->data[(i * oldImage.height) + j] = oldImage.data[i + (oldImage.height - 1 - j) * oldImage.width];
        }
    }
    return 1;
}
